package uet.oop.bomberman.thuc_the;

public abstract class AnimatedEntitiy extends Entity {

	protected int Animate = 0;
	protected final int MAX = 7500;

	protected void animate() {
		if (Animate < MAX)
			Animate++;
		else
			Animate = 0;
	}

}
