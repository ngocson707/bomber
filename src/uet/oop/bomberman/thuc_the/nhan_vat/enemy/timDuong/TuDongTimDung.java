package uet.oop.bomberman.thuc_the.nhan_vat.enemy.timDuong;

import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.Enemy;

public class TuDongTimDung extends random {
	Bomber BOMBER;
	Enemy ENEMY;

	public TuDongTimDung(Bomber bomber, Enemy enemy) {
		BOMBER = bomber;
		ENEMY = enemy;
	}

	@Override
	public int calculateDirection() {
		if (BOMBER == null) {
			return random.nextInt(4);
		}
		int diThang = random.nextInt(2);
		if (diThang == 1) {
			int run = calculateRowDirection();
			if (run != -1) {
				return run;
			} else {
				return calculateColDirection();
			}

		} else {
			int run = calculateColDirection();
			if (run != -1) {
				return run;
			} else {
				return calculateRowDirection();
			}
		}
	}

	protected int calculateColDirection() {
		if (BOMBER.getXTile() < ENEMY.getXTile()) {
			return 3;
		} else if (BOMBER.getXTile() > ENEMY.getXTile()) {
			return 1;
		}
		return -1;
	}

	protected int calculateRowDirection() {
		if (BOMBER.getYTile() < ENEMY.getYTile()) {
			return 0;
		} else if (BOMBER.getYTile() > ENEMY.getYTile()) {
			return 2;
		}
		return -1;
	}

}
