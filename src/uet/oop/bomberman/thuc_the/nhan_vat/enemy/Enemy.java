package uet.oop.bomberman.thuc_the.nhan_vat.enemy;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.Coordinates;

import java.awt.*;
import uet.oop.bomberman.sound.Sound;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.Message;
import uet.oop.bomberman.thuc_the.bomb.Flame;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;
import uet.oop.bomberman.thuc_the.nhan_vat.Character;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.timDuong.random;

public abstract class Enemy extends Character {

	protected int POINT;
	protected final double tang;
	protected final double dung;
	protected double Tang;
	protected double tocDo;
	protected random timDuong;
	protected int finalAnimation = 30;
	protected Sprite sprite_chet;

	public Enemy(int x, int y, Board board, Sprite dead, double speed, int points) {
		super(x, y, board);
		POINT = points;
		tocDo = speed;
		tang = Game.TILES_SIZE / tocDo;
		dung = (tang - (int) tang) / tang;
		Tang = tang;
		timeSau = 20;
		sprite_chet = dead;
	}

	protected abstract void chooseSprite();

	@Override
	public void update() {
		animate();

		if (!song) {
			afterKill();
			return;
		}

		if (song) {
			calculateMove();
		}
	}

	@Override
	public void render(Screen screen) {

		if (song) {
			chooseSprite();
		} else {
			if (timeSau > 0) {
				SPRITE = sprite_chet;
				Animate = 0;
			} else {
				SPRITE = Sprite.movingSprite(Sprite.mob_dead1, Sprite.mob_dead2, Sprite.mob_dead3, Animate, 60);
			}

		}
		screen.renderEntity((int) X, (int) Y - SPRITE.SIZE, this);
	}

	@Override
	public void calculateMove() {

		int xX = 0, yY = 0;
		if (Tang <= 0) {
			pHuong = timDuong.calculateDirection();
			Tang = tang;
		}

		if (pHuong == 0) {
			yY--;
		}
		if (pHuong == 2) {
			yY++;
		}
		if (pHuong == 3) {
			xX--;
		}
		if (pHuong == 1) {
			xX++;
		}

		if (canMove(xX, yY)) {
			Tang -= 1 + dung;
			move(xX * tocDo, yY * tocDo);
			diChuyen = true;
		} else {
			Tang = 0;
			diChuyen = false;
		}
	}

	@Override
	public void move(double x, double y) {
		if (!song) {
			return;
		}
		Y += y;
		X += x;
	}

	@Override
	public boolean canMove(double x, double y) {
		double cx = X, cy = Y - 16;

		if (pHuong == 0) {
			cy += SPRITE.getSize() - 1;
			cx += SPRITE.getSize() / 2;
		}
		if (pHuong == 1) {
			cy += SPRITE.getSize() / 2;
			cx += 1;
		}
		if (pHuong == 2) {
			cx += SPRITE.getSize() / 2;
			cy += 1;
		}
		if (pHuong == 3) {
			cx += SPRITE.getSize() - 1;
			cy += SPRITE.getSize() / 2;
		}

		int XXX = Coordinates.pixelToTile(cx) + (int) x;
		int YYY = Coordinates.pixelToTile(cy) + (int) y;

		Entity entity = BOARD.getEntity(XXX, YYY, this);

		return entity.collide(this);
	}

	@Override
	public boolean collide(Entity entity) {
		if (entity instanceof Flame) {
			this.kill();
			return false;
		}
		if (entity instanceof Bomber) {
			((Bomber) entity).kill();
			return false;
		}
		return true;
	}

	@Override
	public void kill() {
		if (!song) {
			return;
		}

		song = false;
		BOARD.addPoints(POINT);
		Message msg = new Message("+" + POINT, getXMessage(), getYMessage(), 2, Color.white, 14);
		BOARD.addMessage(msg);
		Sound.play("aa"); // am thanh quai chet
	}

	@Override
	protected void afterKill() {
		if (timeSau > 0) {
			--timeSau;
		} else {
			if (finalAnimation > 0) {
				--finalAnimation;
			} else {
				remove();
			}
		}
	}

}
