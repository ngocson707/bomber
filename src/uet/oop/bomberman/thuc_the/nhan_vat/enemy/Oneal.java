package uet.oop.bomberman.thuc_the.nhan_vat.enemy;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.timDuong.Random4;
public class Oneal extends Enemy {
	public Oneal(int x, int y, Board board) {
		super(x, y, board, Sprite.balloom_dead, 0.9, 150);
		SPRITE = Sprite.balloom_left1;
		timDuong = new Random4();
		pHuong = timDuong.calculateDirection();
	}

	@Override
	protected void chooseSprite() {
		switch (pHuong) {
		case 1:
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.oneal_right1, Sprite.oneal_right2, Sprite.oneal_right3, Animate,
						60);
			} else {
				SPRITE = Sprite.oneal_left1;
			}
			break;
		case 3:
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.oneal_left1, Sprite.oneal_left2, Sprite.oneal_left3, Animate, 60);
			} else {
				SPRITE = Sprite.oneal_left1;
			}
			break;
		}
	}
}
