package uet.oop.bomberman.thuc_the.nhan_vat.enemy;
import uet.oop.bomberman.Board;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.timDuong.Random4;

public class Balloon extends Enemy {
	public Balloon(int x, int y, Board board) {
		super(x, y, board, Sprite.balloom_dead, 0.5, 100);
		SPRITE = Sprite.balloom_left1;
		timDuong = new Random4();
		pHuong = timDuong.calculateDirection();
	}

	@Override
	protected void chooseSprite() {
		switch (pHuong) {
		case 1:
			SPRITE = Sprite.movingSprite(Sprite.balloom_right1, Sprite.balloom_right2, Sprite.balloom_right3, Animate,60);
			break;
		case 3:
			SPRITE = Sprite.movingSprite(Sprite.balloom_left1, Sprite.balloom_left2, Sprite.balloom_left3, Animate, 60);
			break;
		}
	}
}
