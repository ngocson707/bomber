package uet.oop.bomberman.thuc_the.nhan_vat.enemy;
import uet.oop.bomberman.Board;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.timDuong.TuDongTimDung;
public class Doll extends Enemy {
	public Doll(int x, int y, Board board) {
		super(x, y, board, Sprite.balloom_dead, 1, 200);
		SPRITE = Sprite.balloom_left1;
		timDuong = new TuDongTimDung(BOARD.getBomber(), this);
		pHuong = timDuong.calculateDirection();
	}

	@Override
	protected void chooseSprite() {
		switch (pHuong) {
		case 1:
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.doll_right1, Sprite.doll_right2, Sprite.doll_right3, Animate, 60);
			} else {
				SPRITE = Sprite.doll_left1;
			}
			break;
		case 3:
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.doll_left1, Sprite.doll_left2, Sprite.doll_left3, Animate, 60);
			} else {
				SPRITE = Sprite.doll_left1;
			}
			break;
		}
	}
}
