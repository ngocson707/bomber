package uet.oop.bomberman.thuc_the.nhan_vat;

import java.util.ArrayList;
import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.Keys.Keyboard;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;

import java.util.Iterator;
import java.util.List;

import uet.oop.bomberman.level.Coordinates;
import uet.oop.bomberman.sound.Sound;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.LayeredEntity;
import uet.oop.bomberman.thuc_the.bomb.Bomb;
import uet.oop.bomberman.thuc_the.bomb.Flame;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.Enemy;
import uet.oop.bomberman.thuc_the.tuong.item.Item;

public class Bomber extends Character {

	private List<Bomb> BOMB;
	protected Keyboard INPUT;
	public static List<Item> ITEM = new ArrayList<Item>();
	protected int time_dat_bom = 0;

	public Bomber(int x, int y, Board board) {
		super(x, y, board);
		BOMB = BOARD.getBombs();
		INPUT = BOARD.getInput();
		SPRITE = Sprite.player_right;
	}

	public void calculateXOffset() {
		int xDc = Screen.calculateXOffset(BOARD, this);
		Screen.setOffset(xDc, 0);
	}

	private void detectPlaceBomb() {

		if (INPUT.space && Game.getBombRate() > 0 && time_dat_bom < 0) {

			int x = Coordinates.pixelToTile(X + SPRITE.getSize() / 2);
			int y = Coordinates.pixelToTile((Y + SPRITE.getSize() / 2) - SPRITE.getSize());
			placeBomb(x, y);
			Game.addBombRate(-1);
			time_dat_bom = 30;
		}
	}

	protected void placeBomb(int x, int y) {
		Bomb b = new Bomb(x, y, BOARD);
		BOARD.addBomb(b);
		Sound.play("set"); // am thanh nhat item b
	}

	private void clearBombs() {
		Iterator<Bomb> iterator = BOMB.iterator();

		Bomb bomb;
		while (iterator.hasNext()) {
			bomb = iterator.next();
			if (bomb.isRemoved()) {
				iterator.remove();
				Game.addBombRate(1);
			}
		}
	}

	private void chooseSprite() {
		switch (pHuong) {
		case 0:
			SPRITE = Sprite.player_up;
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.player_up_1, Sprite.player_up_2, Animate, 20);
			}
			break;
		case 1:
			SPRITE = Sprite.player_right;
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.player_right_1, Sprite.player_right_2, Animate, 20);
			}
			break;
		case 2:
			SPRITE = Sprite.player_down;
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.player_down_1, Sprite.player_down_2, Animate, 20);
			}
			break;
		case 3:
			SPRITE = Sprite.player_left;
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.player_left_1, Sprite.player_left_2, Animate, 20);
			}
			break;
		default:
			SPRITE = Sprite.player_right;
			if (diChuyen) {
				SPRITE = Sprite.movingSprite(Sprite.player_right_1, Sprite.player_right_2, Animate, 20);
			}
			break;
		}
	}

	@Override
	public void update() {
		clearBombs();
		if (!song) {
			afterKill();
			return;
		}

		if (time_dat_bom < -8000) {
			time_dat_bom = 0;
		} else {
			time_dat_bom--;
		}

		animate();
		calculateMove();
		detectPlaceBomb();
	}

	@Override
	public void render(Screen screen) {
		calculateXOffset();

		if (song) {
			chooseSprite();
		} else {
			SPRITE = Sprite.player_dead1;
		}
		screen.renderEntity((int) X, (int) Y - SPRITE.SIZE, this);
	}

	@Override
	public void kill() {
		if (!song)
			return;
		song = false;
		Sound.play("haha"); // am thanh thua bi chet
		// Sound.play("endgame");
	}

	@Override
	protected void afterKill() {
		if (timeSau > 0) {
			--timeSau;
		} else {
			BOARD.endGame();
		}
	}

	@Override
	protected void calculateMove() {
		int lon = 0, bo = 0;
		if (INPUT.up) {
			bo--;
		}
		if (INPUT.down) {
			bo++;
		}
		if (INPUT.left) {
			lon--;
		}
		if (INPUT.right) {
			lon++;
		}

		if (lon != 0 || bo != 0) {
			move(lon * Game.getBomberSpeed(), bo * Game.getBomberSpeed());
			diChuyen = true;
		} else {
			diChuyen = false;
		}
	}

	@Override
	public boolean canMove(double x, double y) {

		for (int i = 0; i < 4; i++) {
			double ax = ((X + x) + i % 2 * 9) / Game.TILES_SIZE;
			double ay = ((Y + y) + i / 2 * 10 - 13) / Game.TILES_SIZE;

			Entity entity = BOARD.getEntity(ax, ay, this);

			if (!entity.collide(this)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void move(double lon, double bo) {
		if (lon > 0) {
			pHuong = 1;
		}
		if (lon < 0) {
			pHuong = 3;
		}
		if (bo > 0) {
			pHuong = 2;
		}
		if (bo < 0) {
			pHuong = 0;
		}

		if (canMove(0, bo)) {
			Y += bo;
		}

		if (canMove(lon, 0)) {
			X += lon;
		}
	}

	@Override
	public boolean collide(Entity entity) {
		if (entity instanceof Flame) {
			this.kill();
			return false;
		}
		if (entity instanceof Enemy) {
			this.kill();
			return true;
		}
		if (entity instanceof LayeredEntity) {
			return (entity.collide(this));
		}
		return true;
	}

}
