package uet.oop.bomberman.thuc_the.nhan_vat;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.thuc_the.AnimatedEntitiy;


public abstract class Character extends AnimatedEntitiy {

	protected boolean song = true;
	protected boolean diChuyen = false;
	public int timeSau = 40;
	protected Board BOARD;
	protected int pHuong = -1;

	public Character(int x, int y, Board board) {
		X = x;
		Y = y;
		BOARD = board;
	}

	@Override
	public abstract void update();

	@Override
	public abstract void render(Screen screen);

	public abstract void kill();

	protected abstract void calculateMove();

	protected abstract void move(double xa, double ya);

	protected double getXMessage() {
		return (X * Game.SCALE) + (SPRITE.SIZE / 2 * Game.SCALE);
	}

	protected double getYMessage() {
		return (Y * Game.SCALE) - (SPRITE.SIZE / 2 * Game.SCALE);
	}

	protected abstract void afterKill();

	protected abstract boolean canMove(double x, double y);

}
