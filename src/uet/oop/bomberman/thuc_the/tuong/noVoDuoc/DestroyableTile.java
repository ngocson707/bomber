package uet.oop.bomberman.thuc_the.tuong.noVoDuoc;

import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.bomb.Flame;
import uet.oop.bomberman.thuc_the.tuong.Tile;

/**
 * Đối tượng cố định có thể bị phá hủy
 */
public class DestroyableTile extends Tile {

	private final int MAX_ANIMATE = 7500;
	private int ANIMATE = 0;
	protected boolean phaHuy = false;
	protected int time_bien_mat = 20;
	protected Sprite duoi_sprite = Sprite.grass;

	public DestroyableTile(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	@Override
	public void update() {
		if (phaHuy) {
			if (ANIMATE < MAX_ANIMATE)
				ANIMATE++;
			else
				ANIMATE = 0;
			if (time_bien_mat > 0)
				time_bien_mat--;
			else
				remove();
		}
	}

	public void destroy() {
		phaHuy = true;
	}

	@Override
	public boolean collide(Entity e) {
		if (e instanceof Flame) {
			destroy();
		}
		return false;
	}

	public void addBelowSprite(Sprite sprite) {
		duoi_sprite = sprite;
	}

	protected Sprite movingSprite(Sprite normal, Sprite a, Sprite b) {
		int tinh = ANIMATE % 30;
		if (tinh < 10) {
			return normal;
		}
		if (tinh < 20) {
			return a;
		}
		return b;
	}

}
