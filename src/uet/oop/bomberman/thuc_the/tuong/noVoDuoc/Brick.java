package uet.oop.bomberman.thuc_the.tuong.noVoDuoc;

import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.Coordinates;

public class Brick extends DestroyableTile {

	public Brick(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void render(Screen screen) {
		int x = Coordinates.tileToPixel(X);
		int y = Coordinates.tileToPixel(Y);

		if (phaHuy) {
			SPRITE = movingSprite(Sprite.brick_exploded, Sprite.brick_exploded1, Sprite.brick_exploded2);
			screen.renderEntityWithBelowSprite(x, y, this, duoi_sprite);
		} else {
			screen.renderEntity(x, y, this);
		}
	}

}
