package uet.oop.bomberman.thuc_the.tuong;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sound.Sound;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;
import uet.oop.bomberman.thuc_the.tuong.item.Item;

public class Portal extends Tile {
	protected Board BOARD;

	public Portal(int x, int y, Board board, Sprite sprite) {
		super(x, y, sprite);
		BOARD = board;
	}

	@Override
	public boolean collide(Entity entity) {

		if (entity instanceof Bomber) {

			if (BOARD.detectNoEnemies() == false) {
				return false;
			}
			
			if (entity.getXTile() == getX() && entity.getYTile() == getY()) {
				if (BOARD.detectNoEnemies()) {
					BOARD.nextLevel();
					Sound.play("upleve");										 // am thanh phat qua cua
				}
			}
			return true;
		}
		return true;
	}
}
