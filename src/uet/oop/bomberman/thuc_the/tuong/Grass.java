package uet.oop.bomberman.thuc_the.tuong;


import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.thuc_the.Entity;

public class Grass extends Tile {

	public Grass(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	@Override
	public boolean collide(Entity e) {
		return true;
	}
}
