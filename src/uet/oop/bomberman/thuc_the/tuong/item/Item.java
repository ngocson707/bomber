package uet.oop.bomberman.thuc_the.tuong.item;

import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;
import uet.oop.bomberman.thuc_the.tuong.Tile;


public abstract class Item extends Tile {
	protected int truongDo = -1;
	protected boolean hanhDong = false;
	protected int LEVEL;

	public Item(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

}
