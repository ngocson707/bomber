package uet.oop.bomberman.thuc_the.tuong.item;

import uet.oop.bomberman.Game;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.sound.Sound;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;

public class BombItem extends Item {

	public BombItem(int x, int y, Sprite sprite) {
		super(x, y, sprite);
	}

	@Override
	public boolean collide(Entity entity) {
		if (entity instanceof Bomber) {
			Sound.play("eat");
			Game.addBombRate(1);
			remove();
		}
		return false;
	}

}
