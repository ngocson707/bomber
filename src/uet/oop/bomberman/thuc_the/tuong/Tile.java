package uet.oop.bomberman.thuc_the.tuong;

import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.Coordinates;
import uet.oop.bomberman.thuc_the.Entity;

public abstract class Tile extends Entity {
	
	public Tile(int x, int y, Sprite sprite) {
		X = x;
		Y = y;
		SPRITE = sprite;
	}

	@Override
	public boolean collide(Entity entity) {
		return false;
	}
	
	@Override
	public void render(Screen screen) {
		screen.renderEntity( Coordinates.tileToPixel(X), Coordinates.tileToPixel(Y), this);
	}
	
	@Override
	public void update() {}
}
