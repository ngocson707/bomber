package uet.oop.bomberman.thuc_the.bomb;

import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.Enemy;

public class FlameSegment extends Entity {

	protected boolean _last;

	public FlameSegment(int x, int y, int direction, boolean last) {
		X = x;
		Y = y;
		_last = last;

		switch (direction) {
		case 0:
			if (!last) {
				SPRITE = Sprite.explosion_vertical2;
			} else {
				SPRITE = Sprite.explosion_vertical_top_last2;
			}
			break;
		case 1:
			if (!last) {
				SPRITE = Sprite.explosion_horizontal2;
			} else {
				SPRITE = Sprite.explosion_horizontal_right_last2;
			}
			break;
		case 2:
			if (!last) {
				SPRITE = Sprite.explosion_vertical2;
			} else {
				SPRITE = Sprite.explosion_vertical_down_last2;
			}
			break;
		case 3:
			if (!last) {
				SPRITE = Sprite.explosion_horizontal2;
			} else {
				SPRITE = Sprite.explosion_horizontal_left_last2;
			}
			break;
		}
	}

	@Override
	public void render(Screen screen) {
		int xt = (int) X << 4;
		int yt = (int) Y << 4;

		screen.renderEntity(xt, yt, this);
	}

	@Override
	public void update() {
	}

	@Override
	public boolean collide(Entity e) {
		if (e instanceof Bomber)
			((Bomber) e).kill();
		if (e instanceof Enemy)
			((Enemy) e).kill();
		return true;
	}

}