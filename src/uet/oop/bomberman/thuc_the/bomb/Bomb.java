package uet.oop.bomberman.thuc_the.bomb;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.Coordinates;
import uet.oop.bomberman.sound.Sound;
import uet.oop.bomberman.thuc_the.AnimatedEntitiy;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;
import uet.oop.bomberman.thuc_the.nhan_vat.Character;

public class Bomb extends AnimatedEntitiy {

	protected double time_no = 70;
	public int sauTime = 11;

	protected Board BOARD;
	protected Flame[] lua;
	protected boolean noBumm = false;
	protected boolean thong_qua = true;

	public Bomb(int x, int y, Board board) {
		X = x;
		Y = y;
		BOARD = board;
		SPRITE = Sprite.bomb;
	}

	@Override
	public void update() {
		if (time_no > 0) {
			time_no--;
		} else {
			if (!noBumm) {
				explode();
			} else {
				updateFlames();
			}

			if (sauTime > 0) {
				sauTime--;
			} else {
				remove();
			}
		}
		animate();
	}

	@Override
	public void render(Screen screen) {
		if (noBumm) {
			SPRITE = Sprite.bomb_exploded2;
			renderFlames(screen);
		} else {
			SPRITE = Sprite.movingSprite(Sprite.bomb, Sprite.bomb_1, Sprite.bomb_2, Animate, 60);
		}

		int xEntity = (int) X << 4;
		int yEntity = (int) Y << 4;
		screen.renderEntity(xEntity, yEntity, this);
	}

	public void renderFlames(Screen screen) {
		for (int i = 0; i < lua.length; i++) {
			lua[i].render(screen);
		}
	}

	public void updateFlames() {
		for (int i = 0; i < lua.length; i++) {
			lua[i].update();
		}
	}

	protected void explode() {
		noBumm = true;
		thong_qua = true;

		Character character = BOARD.getCharacterAtExcluding((int) X, (int) Y, null);
		if (character != null) {
			character.kill();
		}

		lua = new Flame[4];
		for (int i = 0; i < lua.length; i++) {
			lua[i] = new Flame((int) X, (int) Y, i, Game.getBombRadius(), BOARD);
		}
		Sound.play("bombum"); // Am thanh bom no
	}

	public void timeNo() {
		time_no = 0;
	}

	public FlameSegment flameAt(int x, int y) {
		if (!noBumm)
			return null;

		for (int i = 0; i < lua.length; i++) {
			if (lua[i] == null) {
				return null;
			}
			FlameSegment entity = lua[i].flameSegmentAt(x, y);
			if (entity != null) {
				return entity;
			}
		}

		return null;
	}

	@Override
	public boolean collide(Entity entity) {

		if (entity instanceof Bomber) {

			double dX = entity.getX() - Coordinates.tileToPixel(getX());
			double dY = entity.getY() - Coordinates.tileToPixel(getY());

			if (!(dX >= -10 && dX < 16 && dY >= 1 && dY <= 28)) {
				thong_qua = false;
			}
			return thong_qua;
		}

		if (entity instanceof Flame) {
			timeNo();
			return true;
		}
		return false;
	}
}
