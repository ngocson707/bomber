package uet.oop.bomberman.thuc_the;

import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.thuc_the.tuong.noVoDuoc.DestroyableTile;

import java.util.LinkedList;

public class LayeredEntity extends Entity {

	protected LinkedList<Entity> Entities = new LinkedList<>();

	public LayeredEntity(int x, int y, Entity... entities) {
		X = x;
		Y = y;

		for (int i = 0; i < entities.length; i++) {
			Entities.add(entities[i]);

			if (i > 1) {
				if (entities[i] instanceof DestroyableTile) {
					((DestroyableTile) entities[i]).addBelowSprite(entities[i - 1].getSprite());
				}
			}
		}
	}
	

	public Entity getTopEntity() {

		return Entities.getLast();
	}

	private void clearRemoved() {
		
		Entity topEntity = getTopEntity();

		if (topEntity.isRemoved()) {
			Entities.removeLast();
		}
	}

	public void addBeforeTop(Entity entity) {
		Entities.add(Entities.size() - 1, entity);
	}

	@Override
	public boolean collide(Entity entity) {
		return getTopEntity().collide(entity);
	}

	@Override
	public void render(Screen screen) {
		getTopEntity().render(screen);
	}
	
	@Override
	public void update() {
		clearRemoved();
		getTopEntity().update();
	}
	
}
