package uet.oop.bomberman.thuc_the;

import uet.oop.bomberman.graphics.IRender;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.level.Coordinates;

public abstract class Entity implements IRender {

	protected double X, Y;
	protected boolean REMOVE = false;
	protected Sprite SPRITE;

	@Override
	public abstract void update();

	@Override
	public abstract void render(Screen screen);

	public void remove() {
		REMOVE = true;
	}

	public boolean isRemoved() {
		return REMOVE;
	}

	public Sprite getSprite() {
		return SPRITE;
	}

	public abstract boolean collide(Entity e);

	public double getX() {
		return X;
	}

	public double getY() {
		return Y;
	}

	public int getXTile() {
		return Coordinates.pixelToTile(X + SPRITE.SIZE / 2);
	}

	public int getYTile() {
		return Coordinates.pixelToTile(Y - SPRITE.SIZE / 2);
	}

}
