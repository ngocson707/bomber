package uet.oop.bomberman.level;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.exceptions.LoadLevelException;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.graphics.Sprite;
import uet.oop.bomberman.thuc_the.LayeredEntity;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.Balloon;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.Doll;
import uet.oop.bomberman.thuc_the.nhan_vat.enemy.Oneal;
import uet.oop.bomberman.thuc_the.tuong.Grass;
import uet.oop.bomberman.thuc_the.tuong.Portal;
import uet.oop.bomberman.thuc_the.tuong.Wall;
import uet.oop.bomberman.thuc_the.tuong.item.BombItem;
import uet.oop.bomberman.thuc_the.tuong.item.FlameItem;
import uet.oop.bomberman.thuc_the.tuong.item.SpeedItem;
import uet.oop.bomberman.thuc_the.tuong.noVoDuoc.Brick;

public class FileLevelLoader extends LevelLoader {

	private static char[][] MAP;

	public FileLevelLoader(Board board, int level) throws LoadLevelException {
		super(board, level);
	}

	@Override
	public void loadLevel(int level) {

		// load gia tri width, height, level, MAP

		List<String> list = new ArrayList<>();

		try {
			FileReader fileReader = new FileReader("res\\levels\\Level" + level + ".txt");

			BufferedReader bufferedRead = new BufferedReader(fileReader);

			String readline = bufferedRead.readLine();

			while (!readline.equals("")) {
				list.add(readline);
				readline = bufferedRead.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		String[] string = list.get(0).trim().split(" ");

		LEVEL = Integer.parseInt(string[0]);
		Height = Integer.parseInt(string[1]);
		Width = Integer.parseInt(string[2]);
		MAP = new char[Height][Width];

		for (int i = 0; i < Height; i++) {
			for (int j = 0; j < Width; j++) {
				MAP[i][j] = list.get(i + 1).charAt(j);
			}
		}
	}

	@Override
	public void createEntities() {

		for (int i = 0; i < getHeight(); i++) {
			for (int j = 0; j < getWidth(); j++) {
				int pos = j + i * getWidth();
				char ch = MAP[i][j];
				switch (ch) {

				// item dat them +1 bom
				case 'b':
					LayeredEntity layer = new LayeredEntity(j, i, new Grass(j, i, Sprite.grass),
							new BombItem(j, i, Sprite.powerup_bombs), new Brick(j, i, Sprite.brick));
					BOARD.addEntity(pos, layer);
					break;

				// item bom no rong
				case 'f':
					layer = new LayeredEntity(j, i, new Grass(j, i, Sprite.grass),
							new FlameItem(j, i, Sprite.powerup_flames), new Brick(j, i, Sprite.brick));
					BOARD.addEntity(pos, layer);
					break;

				// item tang toc
				case 's':
					layer = new LayeredEntity(j, i, new Grass(j, i, Sprite.grass),
							new SpeedItem(j, i, Sprite.powerup_speed), new Brick(j, i, Sprite.brick));
					BOARD.addEntity(pos, layer);
					break;

				// đường đi
				case ' ':
					BOARD.addEntity(pos, new Grass(j, i, Sprite.grass));
					break;
				// tường
				case '#':
					BOARD.addEntity(pos, new Wall(j, i, Sprite.wall));
					break;
				// cửa qua màn
				case 'x':
					BOARD.addEntity(pos, new LayeredEntity(j, i, new Grass(j, i, Sprite.grass),
							new Portal(j, i, BOARD, Sprite.portal), new Brick(j, i, Sprite.brick)));
					break;
				// gạch
				case '*':
					BOARD.addEntity(j + i * Width,
							new LayeredEntity(j, i, new Grass(j, i, Sprite.grass), new Brick(j, i, Sprite.brick)));
					break;
				// Bomber
				case 'p':
					BOARD.addCharacter(new Bomber(Coordinates.tileToPixel(j),
							Coordinates.tileToPixel(i) + Game.TILES_SIZE, BOARD));
					Screen.setOffset(0, 0);
					BOARD.addEntity(j + i * Width, new Grass(j, i, Sprite.grass));
					break;

				// balloon (quái 1)
				case '1':
					BOARD.addCharacter(new Balloon(Coordinates.tileToPixel(j),
							Coordinates.tileToPixel(i) + Game.TILES_SIZE, BOARD));
					BOARD.addEntity(j + i * Width, new Grass(j, i, Sprite.grass));
					break;
				// oneal (quái 2)

				case '2':
					BOARD.addCharacter(
							new Oneal(Coordinates.tileToPixel(j), Coordinates.tileToPixel(i) + Game.TILES_SIZE, BOARD));
					BOARD.addEntity(pos, new Grass(j, i, Sprite.grass));
					break;

				// doll (quái 3)
				case '3':
					BOARD.addCharacter(
							new Doll(Coordinates.tileToPixel(j), Coordinates.tileToPixel(i) + Game.TILES_SIZE, BOARD));
					BOARD.addEntity(j + i * Width, new Grass(j, i, Sprite.grass));
					break;

				// tao duong Random4
				default:
					BOARD.addEntity(pos, new Grass(j, i, Sprite.grass));
					break;

				}
			}
		}
	}
}
