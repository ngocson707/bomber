package uet.oop.bomberman.level;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.exceptions.LoadLevelException;

public abstract class LevelLoader {

	protected int Width = 20, Height = 20;
	protected int LEVEL;
	protected Board BOARD;

	public int getWidth() {
		return Width;
	}

	public int getHeight() {
		return Height;
	}

	public int getLevel() {
		return LEVEL;
	}

	public LevelLoader(Board board, int level) throws LoadLevelException {
		BOARD = board;
		loadLevel(level);
	}

	public abstract void createEntities();

	public abstract void loadLevel(int level) throws LoadLevelException;

}
