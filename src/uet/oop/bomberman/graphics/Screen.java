package uet.oop.bomberman.graphics;

import uet.oop.bomberman.Board;
import uet.oop.bomberman.Game;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;

import java.awt.*;


public class Screen {
	protected int Width, Height;
	public int[] _pixels;
	private int _transparentColor = 0xffff00ff;

	public static int xOs = 0, yOs = 0;

	public Screen(int width, int height) {
		Width = width;
		Height = height;

		_pixels = new int[width * height];

	}

	public void clear() {
		for (int i = 0; i < _pixels.length; i++) {
			_pixels[i] = 0;
		}
	}

	public void renderEntity(int xp, int yp, Entity entity) { // save entity pixels
		xp -= xOs;
		yp -= yOs;
		for (int y = 0; y < entity.getSprite().getSize(); y++) {
			int ya = y + yp;
			for (int x = 0; x < entity.getSprite().getSize(); x++) {
				int xa = x + xp;
				if (xa < -entity.getSprite().getSize() || xa >= Width || ya < 0 || ya >= Height)
					break;
				if (xa < 0)
					xa = 0;
				int color = entity.getSprite().getPixel(x + y * entity.getSprite().getSize());
				if (color != _transparentColor)
					_pixels[xa + ya * Width] = color;
			}
		}
	}

	public void renderEntityWithBelowSprite(int xp, int yp, Entity entity, Sprite below) {
		xp -= xOs;
		yp -= yOs;
		for (int y = 0; y < entity.getSprite().getSize(); y++) {
			int ya = y + yp;
			for (int x = 0; x < entity.getSprite().getSize(); x++) {
				int xa = x + xp;
				if (xa < -entity.getSprite().getSize() || xa >= Width || ya < 0 || ya >= Height)
					break;
				if (xa < 0)
					xa = 0;
				int color = entity.getSprite().getPixel(x + y * entity.getSprite().getSize());
				if (color != _transparentColor)
					_pixels[xa + ya * Width] = color;
				else
					_pixels[xa + ya * Width] = below.getPixel(x + y * below.getSize());
			}
		}
	}

	public static void setOffset(int xO, int yO) {
		xOs = xO;
		yOs = yO;
	}

	public static int calculateXOffset(Board board, Bomber bomber) {
		if (bomber == null)
			return 0;
		int temp = xOs;

		double BomberX = bomber.getX() / 16;
		double complement = 0.5;
		int firstBreakpoint = board.getWidth() / 4;
		int lastBreakpoint = board.getWidth() - firstBreakpoint;

		if (BomberX > firstBreakpoint + complement && BomberX < lastBreakpoint - complement) {
			temp = (int) bomber.getX() - (Game.WIDTH / 2);
		}

		return temp;
	}

	public void drawEndGame(Graphics g, int points) {
		g.setColor(Color.black);
		g.fillRect(0, 0, getRealWidth(), getRealHeight());

		Font font = new Font("VnTime", Font.PLAIN, 20 * Game.SCALE);
		g.setFont(font);
		g.setColor(Color.white);
		drawCenteredString("GAME OVER", getRealWidth(), getRealHeight(), g);

		font = new Font("VnTime", Font.PLAIN, 10 * Game.SCALE);
		g.setFont(font);
		g.setColor(Color.yellow);
		drawCenteredString("POINTS: " + points, getRealWidth(), getRealHeight() + (Game.TILES_SIZE * 2) * Game.SCALE,
				g);
	}

	public void drawChangeLevel(Graphics g, int level) {
		g.setColor(Color.black);
		g.fillRect(0, 0, getRealWidth(), getRealHeight());

		Font font = new Font("VnTime", Font.PLAIN, 20 * Game.SCALE);
		g.setFont(font);
		g.setColor(Color.white);
		drawCenteredString("LEVEL " + level, getRealWidth(), getRealHeight(), g);

	}

	public void drawPaused(Graphics g) {
		Font font = new Font("VnTime", Font.PLAIN, 20 * Game.SCALE);
		g.setFont(font);
		g.setColor(Color.white);
		drawCenteredString("PAUSED", getRealWidth(), getRealHeight(), g);

	}

	public void drawCenteredString(String s, int w, int h, Graphics g) {
		FontMetrics fm = g.getFontMetrics();
		int x = (w - fm.stringWidth(s)) / 2;
		int y = (fm.getAscent() + (h - (fm.getAscent() + fm.getDescent())) / 2);
		g.drawString(s, x, y);
	}

	public int getWidth() {
		return Width;
	}

	public int getHeight() {
		return Height;
	}

	public int getRealWidth() {
		return Width * Game.SCALE;
	}

	public int getRealHeight() {
		return Height * Game.SCALE;
	}
}
