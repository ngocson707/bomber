package uet.oop.bomberman.gui;

import uet.oop.bomberman.Game;

import javax.swing.*;
import java.awt.*;

public class InfoPanel extends JPanel {
	private JLabel Points;
	private JLabel Time;

	public InfoPanel(Game game) {
		setLayout(new GridLayout());
		Points = new JLabel("Points: " + game.getBoard().getPoints());
		Points.setForeground(Color.white);
		Points.setHorizontalAlignment(JLabel.CENTER);
		Time = new JLabel("Time: " + game.getBoard().getTime());
		Time.setForeground(Color.white);
		Time.setHorizontalAlignment(JLabel.CENTER);
		add(Time);
		add(Points);
		setBackground(Color.black);
		setPreferredSize(new Dimension(0, 40));
	}

	public void setTime(int time) {
		Time.setText("Time: " + time);
	}

	public void setPoints(int point) {
		Points.setText("Score: " + point);
	}

}
