package uet.oop.bomberman.gui;

import uet.oop.bomberman.Game;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {
	private Game game;
	private JPanel Container;
	private InfoPanel info;
	public GamePanel Pane;

	public Frame() {
		Container = new JPanel(new BorderLayout());
		Pane = new GamePanel(this);
		info = new InfoPanel(Pane.getGame());
		Container.add(info, BorderLayout.PAGE_START);
		Container.add(Pane, BorderLayout.PAGE_END);
		game = Pane.getGame();
		add(Container);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
		game.start();
	}

	public void setTime(int time) {
		info.setTime(time);
	}

	public void setPoints(int points) {
		info.setPoints(points);
	}
}
