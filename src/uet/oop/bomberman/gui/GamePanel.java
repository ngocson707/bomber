package uet.oop.bomberman.gui;

import uet.oop.bomberman.Game;

import javax.swing.*;
import java.awt.*;

public class GamePanel extends JPanel {

	private Game GAME;
	
	public GamePanel(Frame frame) {
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(Game.WIDTH * Game.SCALE, Game.HEIGHT * Game.SCALE));
		GAME = new Game(frame);
		add(GAME);
		GAME.setVisible(true);
		setVisible(true);
		setFocusable(true);
	}

	public Game getGame() {
		return GAME;
	}
	
}
