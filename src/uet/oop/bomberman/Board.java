package uet.oop.bomberman;

import uet.oop.bomberman.Keys.Keyboard;
import uet.oop.bomberman.exceptions.LoadLevelException;
import uet.oop.bomberman.graphics.IRender;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.level.FileLevelLoader;
import uet.oop.bomberman.level.LevelLoader;
import uet.oop.bomberman.thuc_the.Entity;
import uet.oop.bomberman.thuc_the.Message;
import uet.oop.bomberman.thuc_the.bomb.Bomb;
import uet.oop.bomberman.thuc_the.bomb.FlameSegment;
import uet.oop.bomberman.thuc_the.nhan_vat.Bomber;
import uet.oop.bomberman.thuc_the.nhan_vat.Character;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Board implements IRender {
	protected LevelLoader levelLoader;
	protected Game playgame;
	protected Keyboard Keys;
	protected Screen SCREEN;

	public Entity[] Entities;
	public List<Character> Characters = new ArrayList<>();
	protected List<Bomb> Bombs = new ArrayList<>();
	private List<Message> Messages = new ArrayList<>();

	private int screenToShow = -1;

	private int TIME = Game.TIME;
	private int POINTS = Game.POINTS;

	public Board(Game game, Keyboard input, Screen screen) {
		playgame = game;
		Keys = input;
		SCREEN = screen;
		loadLevel(1); // start level 1
	}

	@Override
	public void update() {
		if (playgame.isPaused()) {
			return;
		}
		
		updateEntities();
		updateCharacters();
		updateBombs();
		updateMessages();
		detectEndGame();

		for (int i = 0; i < Characters.size(); i++) {
			Character charact = Characters.get(i);
			if (charact.isRemoved()) {
				Characters.remove(i);
			}
		}
	}

	@Override
	public void render(Screen screen) {
		if (playgame.isPaused()) {
			return;
		}

		int x0 = Screen.xOs >> 4;
		int x1 = (Screen.xOs + screen.getWidth() + Game.TILES_SIZE) / Game.TILES_SIZE;
		int y0 = Screen.yOs >> 4;
		int y1 = (Screen.yOs + screen.getHeight()) / Game.TILES_SIZE;

		for (int y = y0; y < y1; y++) {
			for (int x = x0; x < x1; x++) {
				Entities[x + y * levelLoader.getWidth()].render(screen);
			}
		}

		renderBombs(screen);
		renderCharacter(screen);
	}

	public void loadLevel(int level) {
		TIME = Game.TIME;
		screenToShow = 2;
		playgame.resetScreenDelay();
		playgame.pause();
		Characters.clear();
		Bombs.clear();
		Messages.clear();

		try {
			levelLoader = new FileLevelLoader(this, level);
			Entities = new Entity[levelLoader.getHeight() * levelLoader.getWidth()];

			levelLoader.createEntities();
		} catch (LoadLevelException e) {
			endGame();
		}
	}

	protected void detectEndGame() {
		if (TIME <= 0)
			endGame();
	}

	public void endGame() {
		screenToShow = 1;
		playgame.resetScreenDelay();
		playgame.pause();
	}

	public boolean detectNoEnemies() {
		int count = 0;
		for (int i = 0; i < Characters.size(); i++) {
			if (Characters.get(i) instanceof Bomber == false)
				++count;
		}

		return count == 0;
	}

	public void drawScreen(Graphics graphics) {
		switch (screenToShow) {
		case 1:
			SCREEN.drawEndGame(graphics, POINTS);
			break;
		case 2:
			SCREEN.drawChangeLevel(graphics, levelLoader.getLevel());
			break;
		case 3:
			SCREEN.drawPaused(graphics);
			break;
		}
	}

	public Entity getEntity(double x, double y, Character z) {

		Entity entity = null;

		entity = getFlameSegmentAt((int) x, (int) y);
		if (entity != null)
			return entity;

		entity = getBombAt(x, y);
		if (entity != null)
			return entity;

		entity = getCharacterAtExcluding((int) x, (int) y, z);
		if (entity != null)
			return entity;

		entity = getEntityAt((int) x, (int) y);

		return entity;
	}

	public List<Bomb> getBombs() {
		return Bombs;
	}

	public Bomb getBombAt(double x, double y) {
		Iterator<Bomb> bombs = Bombs.iterator();
		Bomb bomb;
		while (bombs.hasNext()) {
			bomb = bombs.next();
			if (bomb.getX() == (int) x && bomb.getY() == (int) y) {
				return bomb;
			}
		}

		return null;
	}

	public Bomber getBomber() {
		Iterator<Character> iterator = Characters.iterator();

		Character character;
		while (iterator.hasNext()) {
			character = iterator.next();

			if (character instanceof Bomber) {
				return (Bomber) character;
			}
		}

		return null;
	}

	public Character getCharacterAtExcluding(int x, int y, Character charact) {

		Iterator<Character> iterator = Characters.iterator();

		Character character;
		while (iterator.hasNext()) {
			character = iterator.next();
			if (character == charact) {
				continue;
			}

			if (character.getXTile() == x && character.getYTile() == y) {
				return character;
			}

		}

		return null;
	}

	public FlameSegment getFlameSegmentAt(int x, int y) {
		Iterator<Bomb> bombs = Bombs.iterator();
		Bomb bomb;
		while (bombs.hasNext()) {
			bomb = bombs.next();

			FlameSegment flameSegment = bomb.flameAt(x, y);
			if (flameSegment != null) {
				return flameSegment;
			}
		}

		return null;
	}

	public Entity getEntityAt(double x, double y) {
		return Entities[(int) x + (int) y * levelLoader.getWidth()];
	}

	public void addEntity(int pos, Entity entity) {
		Entities[pos] = entity;
	}

	public void addCharacter(Character entity) {
		Characters.add(entity);
	}

	public void addBomb(Bomb bomb) {
		Bombs.add(bomb);
	}

	public void addMessage(Message message) {
		Messages.add(message);
	}

	protected void renderCharacter(Screen screen) {
		Iterator<Character> iterator = Characters.iterator();

		while (iterator.hasNext())
			iterator.next().render(screen);
	}

	protected void renderBombs(Screen screen) {
		Iterator<Bomb> iterator = Bombs.iterator();

		while (iterator.hasNext())
			iterator.next().render(screen);
	}

	public void renderMessages(Graphics graphics) {
		Message message;
		for (int i = 0; i < Messages.size(); i++) {
			message = Messages.get(i);

			graphics.setFont(new Font("VnTime", Font.PLAIN, message.getSize()));
			graphics.setColor(message.getColor());
			graphics.drawString(message.getMessage(), (int) message.getX() - Screen.xOs * Game.SCALE,
					(int) message.getY());
		}
	}

	protected void updateEntities() {
		if (playgame.isPaused()) {
			return;
		}
		for (int i = 0; i < Entities.length; i++) {
			Entities[i].update();
		}
	}

	protected void updateCharacters() {
		if (playgame.isPaused())
			return;
		Iterator<Character> iterator = Characters.iterator();

		while (iterator.hasNext() && !playgame.isPaused())
			iterator.next().update();
	}

	protected void updateBombs() {
		if (playgame.isPaused()) {
			return;
		}
		Iterator<Bomb> iterator = Bombs.iterator();

		while (iterator.hasNext()) {
			iterator.next().update();
		}
	}

	protected void updateMessages() {
		int l;
		if (playgame.isPaused()) {
			return;
		}
		Message message;

		for (int i = 0; i < Messages.size(); i++) {
			message = Messages.get(i);
			l = message.getDuration();

			if (l > 0) {
				message.setDuration(--l);
			} else {
				Messages.remove(i);
			}
		}
	}

	public void nextLevel() {
		Game.setBombRadius(1);
		Game.setBombRate(1);
		Game.setBomberSpeed(1.0);
		loadLevel(levelLoader.getLevel() + 1);
	}

	public int subtractTime() {
		if (playgame.isPaused()) {
			return this.TIME;
		} else {
			return this.TIME--;
		}
	}

	public Keyboard getInput() {
		return Keys;
	}

	public LevelLoader getLevel() {
		return levelLoader;
	}

	public Game getGame() {
		return playgame;
	}

	public int getShow() {
		return screenToShow;
	}

	public void setShow(int i) {
		screenToShow = i;
	}

	public int getTime() {
		return TIME;
	}

	public int getPoints() {
		return POINTS;
	}

	public void addPoints(int points) {
		this.POINTS += points;
	}

	public int getWidth() {
		return levelLoader.getWidth();
	}

	public int getHeight() {
		return levelLoader.getHeight();
	}

}
