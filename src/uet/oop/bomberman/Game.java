package uet.oop.bomberman;

import uet.oop.bomberman.Keys.Keyboard;
import uet.oop.bomberman.graphics.Screen;
import uet.oop.bomberman.gui.Frame;

import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

public class Game extends Canvas {

	public static final int TILES_SIZE = 16, WIDTH = TILES_SIZE * (31 / 2), HEIGHT = 13 * TILES_SIZE;

	public static int SCALE = 3;

	public static final String TITLE = "Play Game";

	private static final int BOMBRATE = 1;
	private static final int BOMBRADIUS = 1;
	private static final double BOMBERSPEED = 1.0;// toc do bomber

	public static final int TIME = 200;
	public static final int POINTS = 0;
	protected static int SCREENDELAY = 3;
	protected static int bombRate = BOMBRATE;
	protected static int bombRadius = BOMBRADIUS;
	protected static double bomberSpeed = BOMBERSPEED;
	protected int screenDelay = SCREENDELAY;
	private Keyboard INPUT;
	private boolean run = false;
	private boolean pause = true;
	private Board board;
	private Screen screen;
	private Frame FRAME;
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

	public static double getBomberSpeed() {
		return bomberSpeed;
	}

	public static int getBombRate() {
		return bombRate;
	}

	public static int getBombRadius() {
		return bombRadius;
	}

	public static void addBomberSpeed(double x) {
		bomberSpeed += x;
	}

	public static void addBombRadius(int x) {
		bombRadius += x;
	}

	public static void addBombRate(int x) {
		bombRate += x;
	}

	public void resetScreenDelay() {
		screenDelay = SCREENDELAY;
	}

	public Board getBoard() {
		return board;
	}

	public boolean isPaused() {
		return pause;
	}

	public void pause() {
		pause = true;
	}

	public static void setBombRate(int bombRate) {
		Game.bombRate = bombRate;
	}

	public static void setBombRadius(int bombRadius) {
		Game.bombRadius = bombRadius;
	}

	public static void setBomberSpeed(double bomberSpeed) {
		Game.bomberSpeed = bomberSpeed;
	}

	public Game(Frame frame) {
		FRAME = frame;
		FRAME.setTitle(TITLE);

		screen = new Screen(WIDTH, HEIGHT);
		INPUT = new Keyboard();

		board = new Board(this, INPUT, screen);
		addKeyListener(INPUT);
	}

	public void start() {
		run = true;
		int updates = 0;
		double count = 0;
		int frm = 0;
		long timer = System.currentTimeMillis();
		long lTime = System.nanoTime();
		final double nsecond = 1000000000.0 / 60.0;
		requestFocus();
		while (run) {
			long now = System.nanoTime();
			count += (now - lTime) / nsecond;
			lTime = now;
			while (count >= 1) {
				update();
				updates++;
				count--;
			}
			if (pause) {
				if (screenDelay <= 0) {
					board.setShow(-1);
					pause = false;
				}
				renderScreen();
			} else {
				renderGame();
			}

			frm ++;

			if (System.currentTimeMillis() - timer > 1000) {
				FRAME.setTime(board.subtractTime());
				FRAME.setPoints(board.getPoints());
				timer += 1000;
				FRAME.setTitle(TITLE + "   |   " + " FPS: " + updates + "   -   RATE: " + frm + "    |   Play: 1   |");
				updates = 0;
				frm = 0;
				
				if (board.getShow() == 2) {
					-- screenDelay;
				}
			}
		}
	}

	private void renderGame() {
		BufferStrategy bufferStrategy = getBufferStrategy();
		if (bufferStrategy == null) {
			createBufferStrategy(3);
			return;
		}

		screen.clear();

		board.render(screen);

		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = screen._pixels[i];
		}
		Graphics graphics = bufferStrategy.getDrawGraphics();
		graphics.drawImage(image, 0, 0, getWidth(), getHeight(), null);
		board.renderMessages(graphics);
		graphics.dispose();
		bufferStrategy.show();
	}

	private void renderScreen() {
		BufferStrategy bufferStrategy = getBufferStrategy();
		if (bufferStrategy == null) {
			createBufferStrategy(3);
			return;
		}
		screen.clear();
		Graphics graphics = bufferStrategy.getDrawGraphics();
		board.drawScreen(graphics);
		graphics.dispose();
		bufferStrategy.show();
	}

	private void update() {
		INPUT.update();
		board.update();
	}

}
